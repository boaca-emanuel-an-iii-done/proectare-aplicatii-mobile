package com.example.rssviwer;

import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RssReader {
    public static void setPath(String pat) {
        if(path=="")
        path = pat;
    }

    private static String path="";
    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    String title="",image="",date="";int item;
  //  String image,build,generator,copyright,description,link,title;
    public boolean quickread(String feedUrl) {

        try {
            URL u = new URL(feedUrl);
            HttpURLConnection conn = (HttpURLConnection) u.openConnection();
            conn.setRequestProperty("Accept-Encoding", "identity");
            conn.setRequestMethod("GET");
            conn.setDoOutput(true);
            conn.connect();

            InputStream stream=conn.getInputStream();



            XmlPullParser myparser = XmlPullParserFactory.newInstance().newPullParser();

            myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            myparser.setInput(stream,null);

           boolean rezult= quickRead(myparser);
            stream.close();
       //     f.delete();

            return rezult;


        } catch (Exception e) {
            Log.println(Log.ASSERT, "A", e.getMessage());

        }
     //   f.delete();
        return true;
    }
        private boolean quickRead(XmlPullParser xml)
        {
            int event;
            String name="";
            String text="";
            int rss=0,channel=0,items=0;
           try {
               event=xml.getEventType();
               while(event!=XmlPullParser.END_DOCUMENT) {
                   name = xml.getName();
                   switch (event) {
                       case XmlPullParser.START_TAG:
                           if (name.equals("rss")) {
                               rss = xml.getDepth();
                           }
                           if (name.equals("title")) {
                               if(rss+2==xml.getDepth())

                                   title = getText(xml);
                           }
                           if (name.equals("url"))
                           {
                               image=getText(xml);
                           }


                       case XmlPullParser.TEXT:
                           text = xml.getText();
                           break;


                       case XmlPullParser.END_TAG:
                            if(name.equals("item"))
                                items++;
                            if(name.equals("lastBuildDate"))
                                date=text;

                           break;
                   }

                   event=xml.next();
               }
           } catch (XmlPullParserException | IOException e) {
               Log.println(Log.ASSERT, "error", e.getMessage());
               title= "Eroare:Adresa specificata nu contine un fisier valid";
               return false;
           }
           item=items;
           return true;
        }

    public List<RssItem> getItems(String feedUrl)
    {
        String title = "",link="",description="",imageUrl="";
        ArrayList<RssItem> returnList=new ArrayList<>() ;
        try {
            URL u = new URL(feedUrl);
            HttpURLConnection conn = (HttpURLConnection) u.openConnection();
            conn.setRequestMethod("GET");
            conn.setDoOutput(true);
            conn.connect();

            InputStream stream = conn.getInputStream();


            XmlPullParser xml = XmlPullParserFactory.newInstance().newPullParser();

            xml.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            xml.setInput(stream, null);

           int event=xml.getEventType();
            Log.println(Log.ASSERT, "B", String.valueOf(xml.getEventType()));
           String name="",text=""; int rss=0;
            while(event!=XmlPullParser.END_DOCUMENT) {
                name = xml.getName();

                if(name==null)
                    name="";
                switch (event) {
                    case XmlPullParser.START_TAG:
                        if (name.equals("item")) {
                            rss = xml.getDepth();
                        }
                        if (name.equals("enclosure")) {

                            imageUrl = xml.getAttributeValue(null, "url");
                        }
                        break;


                    case XmlPullParser.TEXT:
                        text = xml.getText();
                        break;
                    case XmlPullParser.END_TAG:

                        if (name.equals("title")) {
                            if(rss+1==xml.getDepth())
                                title = text;
                        }

                        if (name.equals("item"))
                        {
                            returnList.add(new RssItem(title,link,description,imageUrl));
                        }


                        if (name.equals("link"))
                        {
                            link=text;
                        }


                        if (name.equals("description"))
                        {
                            description=text;
                        }




                        break;
                    default: break;
                }
                event=xml.next();
            }


            stream.close();

            conn.disconnect();



        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.println(Log.ASSERT, "B", String.valueOf(returnList.size()));
        return  returnList;
    }
    private String getText(XmlPullParser xml) throws IOException, XmlPullParserException {
        String aux="";
        try

{
  aux = xml.nextText();


    if (aux == "") {
        int evt = xml.nextToken();
        if (evt == XmlPullParser.CDSECT) {
            aux = xml.getText();

        }
    }
} catch ( Exception e)
{
    e.printStackTrace();
}
return aux;
    }
}
