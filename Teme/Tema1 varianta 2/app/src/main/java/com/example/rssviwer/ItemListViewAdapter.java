package com.example.rssviwer;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ItemListViewAdapter extends ArrayAdapter<RssItem> {
    private final Context context;

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    private Activity activity=null;
    public ItemListViewAdapter(@NonNull Context context, int resource, @NonNull List<RssItem> objects) {
        super(context, resource, objects);
        this.context=context;
        this.items=objects;
    }

    List<RssItem> items=new ArrayList<>();
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView=null;

        if(position%2==0)
            itemView =inflater.inflate(R.layout.rss_view_item,parent,false);
        else
            itemView =inflater.inflate(R.layout.rss_view_item_1,parent,false);

        TextView title=itemView.findViewById(R.id.textView);
        final ImageView icon =(ImageView)itemView.findViewById(R.id.imageView);
        title.setText(items.get(position).title);
        final String imageurl=items.get(position).imageUrl;
        TextView description= itemView.findViewById(R.id.textView2);
        description.setText(items.get(position).description);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(imageurl);
                    Bitmap limage = BitmapFactory.decodeStream(url.openConnection().getInputStream());

                    final Bitmap image = Bitmap.createScaledBitmap(limage, 100, 100, false);

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            icon.setImageBitmap(image);

                        }
                    });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }).start();
        return itemView;

    }
}
