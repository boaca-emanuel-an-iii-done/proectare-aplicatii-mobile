package com.example.rssviwer;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class AddNewRssFeed extends AppCompatActivity {
   private WebView myWebView=null;
   private Button webBack=null;
   private Button webFoword=null;
   private EditText addressbar=null;
   private String address="";
   private Button goButton=null;
   private Button addUrl=null;
   private Button returnButton=null;
   private ArrayList<String>rssURLs=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Intent localIntent=getIntent();
        setContentView(R.layout.activity_add_new_rss_feed);
//wiring
        webBack=(Button)findViewById(R.id.hbackbutton);
        webFoword=(Button)findViewById(R.id.hfowrdbutton);
        myWebView =(WebView)findViewById(R.id.webView);
        addressbar=(EditText) findViewById(R.id.adressbar);
        goButton =(Button) findViewById(R.id.enterbutton);
        addUrl=(Button) findViewById(R.id.AddButton);
        returnButton=(Button) findViewById(R.id.ReturnButton);


        webBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myWebView.goBack();
            }
        });
        webFoword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myWebView.goForward();
            }
        });
        addUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String toAdd=addressbar.getText().toString();
                if(!rssURLs.contains(toAdd)) {
                    rssURLs.add(toAdd);
                    Toast.makeText(AddNewRssFeed.this, toAdd+"\n a fost adaugat", Toast.LENGTH_SHORT).show();
                }
                else Toast.makeText(AddNewRssFeed.this, toAdd+"\n nu a fost adaugat", Toast.LENGTH_SHORT).show();

            }
        });
        myWebView.getSettings().setJavaScriptEnabled(true);

        myWebView.setWebViewClient(new MyWebViewClient(myWebView,addressbar));

        myWebView.loadUrl("http://google.com");

        addressbar.setSelectAllOnFocus(true);
        goButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myWebView.loadUrl(addressbar.getText().toString());
            }
        });
        returnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                localIntent.putStringArrayListExtra("content",rssURLs);
                setResult(Activity.RESULT_OK,localIntent);

                finish();
            }
        });
    }

}