package com.example.rssviwer;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;


public class MainActivity extends AppCompatActivity {

    private FloatingActionButton addfeedbutton=null;
    private int requestcCodeId=Math.abs(new Random().nextInt(30000));
     FeedListViewAdapter listViewAdapter=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RssReader.setPath(getFilesDir().getAbsolutePath());
        addfeedbutton =(FloatingActionButton)findViewById(R.id.floatingActionButton2);
        addfeedbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent= new Intent(v.getContext(),AddNewRssFeed.class);
                startActivityForResult(intent,requestcCodeId);

            }
        });
        final ListView listView=(ListView)findViewById(R.id.FeedList);

               listViewAdapter =new FeedListViewAdapter(MainActivity.this,R.layout.rss_select,FeedsItems.Populate(getFeedList()));
                listViewAdapter.setActivity(this);
                       listView.setAdapter(listViewAdapter);



          }


    @Override
    protected void onActivityResult(int requestCode, final int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==requestcCodeId)
        {

            if(resultCode== Activity.RESULT_OK)
            {

                    final ArrayList<String> newfeed = data.getStringArrayListExtra("content");
                   // Log.println(Log.ASSERT,"Error","a ajuns"+feed.size());


                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ArrayList<String>oldfeed=getFeedList();
                        for(String data:newfeed)
                        if(!oldfeed.contains(data))
                            oldfeed.add(data);
                        addtoList(oldfeed);

                    }
                }).run();


            }
        }
    }
     public void save()
    {
       List<FeedsItems> list= FeedsItems.Populate(new ArrayList<String>());
       ArrayList<String>save=new ArrayList<>();
        for(FeedsItems item:list)
        {
            save.add(item.feedUrl);
        }
        addtoList(save);
    }
    private  void addtoList(ArrayList<String> data)
    {
        File f= new File(getFilesDir().getAbsolutePath()+"feedlist.txt");
        try {

            FileOutputStream read=new FileOutputStream(f,false);
           PrintStream out= new PrintStream(read);
           for (String x: data)
               out.println(x);
           out.close();
           read.close();
           FeedsItems.Populate(getFeedList());
           listViewAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private ArrayList<String> getFeedList()
    {
        ArrayList<String> result=new ArrayList<>();
        File f= new File(getFilesDir().getAbsolutePath()+"feedlist.txt");
        try {
            FileInputStream read=new FileInputStream(f);
            Scanner sc=new Scanner(read);
            while (sc.hasNext())
            {
            String entry=sc.nextLine();
            if(!result.contains(entry))
                result.add(entry);
            }
            sc.close();
            read.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onPause() {
        super.onPause();
        save();
    }
}