package com.example.rssviwer;


import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;

public class MyWebViewClient  extends WebViewClient {
    EditText url=null;

    public MyWebViewClient(WebView view,EditText url)
    {
        this.url=url;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        this.url.setText(url);
    }
}
