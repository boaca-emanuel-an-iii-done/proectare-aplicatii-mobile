package com.example.rssviwer;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class FeedListViewAdapter extends ArrayAdapter<FeedsItems> {
    Context context=null;
    List<FeedsItems> itemsList=new ArrayList<>();

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    private Activity activity=null;
    public FeedListViewAdapter(@NonNull Context context, int resource, @NonNull List<FeedsItems> objects) {
        super(context, resource, objects);
        this.context=context;

        if(objects!=null)
            itemsList=objects;

    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView =inflater.inflate(R.layout.rss_select,parent,false);
         final FeedsItems item=itemsList.get(position);

        final ImageView icon =rowView.findViewById(R.id.icon);
        final TextView title=rowView.findViewById(R.id.feed_title);
        final TextView subtitle=rowView.findViewById(R.id.subtitle);
        final TextView subtitle2=rowView.findViewById(R.id.subtitle2);
        subtitle.setText(item.subtitle1);
        subtitle2.setText(item.subtitle2);
        rowView.findViewById(R.id.RemoveButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                itemsList.remove(position);

                FeedListViewAdapter.this.notifyDataSetChanged();
            }
        });
        title.setText(item.title);

            icon.setImageResource(item.defaultimage);

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context,RssViewer.class);
                intent.putExtra("RssUrl",item.feedUrl);
                intent.putExtra("title",item.title);
                activity.startActivity(intent);
            }
        });
        if(item.icon!=null)
            icon.setImageBitmap(item.icon);
        new Thread(new Runnable() {
            @Override
            public void run() {

               item.quicUpdate();
               if(item.icon==null)
                   try {
                       URL url = new URL(item.imageurl);
                       Bitmap limage = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                       final Bitmap image = Bitmap.createScaledBitmap(limage, 200, 200, false);

                       item.icon=image;
                       activity.runOnUiThread(new Runnable() {
                           @Override
                           public void run() {
                               icon.setImageBitmap(image);
                           }
                       });
                   } catch (MalformedURLException e) {
                       e.printStackTrace();
                   } catch (IOException e) {
                       e.printStackTrace();
                   }


                   activity.runOnUiThread(new Runnable() {
                       @Override
                       public void run() {
                           if (item.title != "")
                               title.setText(item.title);
                       }});
               }

        }).start();

        return rowView;
    }
}
