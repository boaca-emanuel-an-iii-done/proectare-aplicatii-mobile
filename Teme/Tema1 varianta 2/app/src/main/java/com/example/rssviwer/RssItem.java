package com.example.rssviwer;

public class RssItem {
    String title,link,description,imageUrl;

    public RssItem(String title, String link, String description, String imageUrl) {
        this.title = title;
        this.link = link;
        this.description = description;
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
