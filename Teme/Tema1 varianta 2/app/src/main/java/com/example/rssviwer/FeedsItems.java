package com.example.rssviwer;

import android.graphics.Bitmap;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class FeedsItems {
    private FeedsItems(String src)
    {
        if(src!=null)
        feedUrl=src;
    }
    String imageurl="";
    String title="";
    String feedUrl="";
    String subtitle1="";
    String subtitle2="";

    Bitmap icon=null;
    int defaultimage=R.drawable.ic_baseline_rss_feed_24;
    boolean valid=true;
    boolean removed=false;
    private static List<FeedsItems> cakelist=null;
public static List<FeedsItems> Populate(List<String> input){
    if(cakelist==null)
        cakelist=new ArrayList<>();
    for(String item:input) {
       FeedsItems feed= new FeedsItems(item);
        if (!cakelist.contains(feed))
            cakelist.add(feed);
    }
    return cakelist;

}
public static FeedsItems getItem(int index)
{
    return cakelist.get(index);
}
public boolean quicUpdate()
    {
        if(title==""&&valid) {
            RssReader reader = new RssReader();
            reader.quickread(feedUrl);
            subtitle1=String.valueOf(reader.item);
            subtitle2=reader.date;
           title=reader.getTitle();
           imageurl=reader.getImage();
        return true;
        }
return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FeedsItems that = (FeedsItems) o;
        return Objects.equals(feedUrl, that.feedUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(feedUrl);
    }
}
