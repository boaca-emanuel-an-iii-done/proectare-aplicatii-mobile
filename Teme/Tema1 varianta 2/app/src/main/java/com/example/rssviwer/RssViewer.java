package com.example.rssviwer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class RssViewer extends AppCompatActivity {
    String url=null;
    List<RssItem> item=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rss_viewer);
         url=getIntent().getStringExtra("RssUrl");
        TextView title=findViewById(R.id.RssTitle);
        title.setText(getIntent().getStringExtra("title"));
        final ListView listView=(ListView)findViewById(R.id.itemslist);
        final Context context=getApplicationContext();
        new Thread(new Runnable() {
            @Override
            public void run() {
                 item=new RssReader().getItems(url);

                runOnUiThread(new Runnable() {
                                  @Override
                                  public void run() {
                                      ItemListViewAdapter listViewAdapter = new ItemListViewAdapter(
                                           context, R.layout.rss_view_item, item);
                                      listView.setAdapter(listViewAdapter);
                                      listViewAdapter.setActivity(RssViewer.this);
                                  }
                              }
                );
            }
        }).start();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                new moreInfoPopup(item.get(position),RssViewer.this).showPopupWindow(view);
            }
        });

    }
}