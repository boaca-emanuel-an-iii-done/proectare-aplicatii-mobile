package com.example.rssviwer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class moreInfoPopup {
    RssItem item=null;
    Activity activity=null;
    public moreInfoPopup(RssItem item, Activity activity)
    {
        this.item=item;
        this.activity=activity;
    }

    //PopupWindow display method

    public void showPopupWindow(final View view) {


        //Create a View object yourself through inflater
        LayoutInflater inflater = (LayoutInflater) view.getContext().getSystemService(view.getContext().LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.more_info, null);

        //Specify the length and width through constants
        int width = 500;
        int height = 500;

        //Make Inactive Items Outside Of PopupWindow
        boolean focusable = true;

        //Create a window with our parameters
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        //Set the location of the window on the screen
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        //Initialize the elements of our window, install the handler

        TextView title = popupView.findViewById(R.id.title);
        title.setText(item.title);
       final ImageView icon =popupView.findViewById(R.id.image);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(item.imageUrl);
                    final Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());


                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            icon.setImageBitmap(image);

                        }
                    });
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        TextView text = popupView.findViewById(R.id.text);
        text.setText(item.description);
        Button buttonEdit = popupView.findViewById(R.id.button);
        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //As an example, display the message
                Intent intent=new Intent(activity,Htmlview.class);
                intent.putExtra("pageUrl",item.link);
                popupWindow.dismiss();
                activity.startActivity(intent);

            }
        });



        //Handler for clicking on the inactive zone of the window

        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                //Close the window when clicked
                popupWindow.dismiss();
                return true;
            }
        });
    }

}