package com.example.tema1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    private class OnItemclick implements ItemClickListener
    {
        @Override
        public void onItemClick(View view, int position) {
            Intent intent= new Intent(getApplicationContext(),Detali.class);
            Bundle bundle =new Bundle();
            bundle.putInt("selectedItemID",position);
            bundle.putBoolean("Full",false);
            intent.putExtras(bundle);
            startActivity(intent);

        }
    }
}