package com.example.tema1;

import android.net.Uri;

public class SimpleDetails {
    private  String FirstString="";
    private String SecondString="";
    private Uri miniatura=Uri.EMPTY;
    public String getFirstString() {
        return FirstString;
    }

    public String getSecondString() {
        return SecondString;
    }

    public Uri getMiniatura() {
        return miniatura;
    }


}
