package com.example.tema1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class FirstRecycleViewAdapter extends RecyclerView.Adapter<FirstRecycleViewAdapter.ViewHolder> {

    private List<SimpleDetails> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    FirstRecycleViewAdapter(Context context, List<SimpleDetails> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.listlayout, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SimpleDetails sData = mData.get(position);
        holder.image.setImageURI(sData.getMiniatura());
       holder.firstText.setText(sData.getFirstString());
       holder.seccondText.setText(sData.getSecondString());
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView firstText=null,seccondText=null;
        ImageView image=null;

        ViewHolder(View itemView) {
            super(itemView);
            firstText = itemView.findViewById(R.id.FirstText);
            seccondText =itemView.findViewById(R.id.SecondText);
            image =itemView.findViewById(R.id.minatura);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position


    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events

}