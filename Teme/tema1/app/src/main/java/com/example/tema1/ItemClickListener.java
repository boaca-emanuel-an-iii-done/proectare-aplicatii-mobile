package com.example.tema1;

import android.view.View;

public interface ItemClickListener {
    void onItemClick(View view, int position);
}