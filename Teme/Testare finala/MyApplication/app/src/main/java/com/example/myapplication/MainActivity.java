package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView first=findViewById(R.id.first);
        TextView second=findViewById(R.id.second);
        TextView thrd=findViewById(R.id.thrd);
        TextView curent=findViewById(R.id.curent);
        Random rand=new Random();
        first.setText(Integer.toString(Math.abs(rand.nextInt()%50)));
        second.setText(Integer.toString(Math.abs(rand.nextInt()%50)));
        thrd.setText(Integer.toString(Math.abs(rand.nextInt()%50)));

        new Thread(new Runnable() {
            Boolean f=false,s=false,t=false;
            @Override

            public void run() {
                while(true){
                int nr=Math.abs(rand.nextInt()%50);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        curent.setText(Integer.toString(nr));

                        if(Integer.toString(nr).compareTo(first.getText().toString())==0)
                        {
                            first.setBackgroundColor(Color.parseColor("#800080"));
                            f=true;
                            Button b= findViewById(R.id.firstck);
                            b.setEnabled(false);
                        }
                        if(Integer.toString(nr).compareTo(second.getText().toString())==0)
                        {
                            second.setBackgroundColor(Color.parseColor("#800080"));
                            s=true;
                            Button b= findViewById(R.id.secondck);
                            b.setEnabled(false);
                        }
                        if(Integer.toString(nr).compareTo(thrd.getText().toString())==0)
                        {
                            thrd.setBackgroundColor(Color.parseColor("#800080"));
                            t=true;
                            Button b= findViewById(R.id.thrdck);
                            b.setEnabled(false);
                        }
                        if(f&t&s)
                        {
                            Toast.makeText(MainActivity.this,"Well Done",Toast.LENGTH_LONG).show();

                        }
                    }
                });
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                }
            }
        }).start();
    }
}