package com.example.clock;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    Clock12 clock12;
    Clock24 clock24;
    Alarm alarm;
    Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       clock12=(Clock12)(findViewById(R.id.Clock12));
alarm=(Alarm) (findViewById(R.id.Alarm));

timer=(Timer)(findViewById(R.id.Timer));
clock24=(Clock24)(findViewById(R.id.Clock24));;
alarm.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        alarm.set(Calendar.getInstance());
    }
});
    }

    @Override
    protected void onResume() {
        super.onResume();
        clock12.startDrawThread();
        alarm.startDrawThread();
        timer.startDrawThread();
        clock24.startDrawThread();
    }

    @Override
    protected void onPause() {
        clock12.stopDrawThread();
        alarm.startDrawThread();
        timer.startDrawThread();
        clock24.startDrawThread();
        super.onPause();
    }
}