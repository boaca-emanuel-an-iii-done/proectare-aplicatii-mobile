package com.example.clock;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import java.util.Calendar;

/**
 * Defines a custom SurfaceView class which handles the drawing thread
 **/
public class Clock12 extends SurfaceView implements SurfaceHolder.Callback, View.OnTouchListener, Runnable
{

    /**
     * Holds the surface frame
     */
    private SurfaceHolder holder;

    /**
     * Draw thread
     */
    private Thread drawThread;

    /**
     * True when the surface is ready to draw
     */
    private boolean surfaceReady = false;


    /**
     * Drawing thread flag
     */

    private boolean drawingActive = false;

    /**
     * Paint for drawing the sample rectangle
     */
    private Paint samplePaint = new Paint();

    /**
     * Time per frame for 60 FPS
     */


    private static final String LOGTAG = "surface";

    public Clock12(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        setOnTouchListener(this);

        // red
        samplePaint.setColor(0xffffff00);
        // smooth edges
       // samplePaint.setAntiAlias(true);
        samplePaint.setStrokeWidth(100f);
    }

    public Clock12(Context context) {
        super(context);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
    {
        if (width == 0 || height == 0)
        {
            return;
        }

        // resize your UI
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
        this.holder = holder;

        if (drawThread != null)
        {
            Log.d(LOGTAG, "draw thread still active..");
            drawingActive = false;
            try
            {
                drawThread.join();
            } catch (InterruptedException e)
            { // do nothing
            }
        }

        surfaceReady = true;
        startDrawThread();
        Log.d(LOGTAG, "Created");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        // Surface is not used anymore - stop the drawing thread
        stopDrawThread();
        // and release the surface
        holder.getSurface().release();

        this.holder = null;
        surfaceReady = false;
        Log.d(LOGTAG, "Destroyed");
    }

    @Override
    public boolean onTouch(View v, MotionEvent event)
    {
        // Handle touch events

        return true;
    }

    /**
     * Stops the drawing thread
     */
    public void stopDrawThread()
    {
        if (drawThread == null)
        {
            Log.d(LOGTAG, "DrawThread is null");
            return;
        }
        drawingActive = false;
        while (true)
        {
            try
            {
                Log.d(LOGTAG, "Request last frame");
                drawThread.join(5000);
                break;
            } catch (Exception e)
            {
                Log.e(LOGTAG, "Could not join with draw thread");
            }
        }
        drawThread = null;
    }

    /**
     * Creates a new draw thread and starts it.
     */
    public void startDrawThread()
    {
        if (surfaceReady && drawThread == null)
        {
            drawThread = new Thread(this, "Draw thread");
            drawingActive = true;
            drawThread.start();
        }
    }

    @Override
    public void run()
    {

        try
        {
            while (drawingActive)
            {
                if (holder == null)
                {
                    return;
                }



                Canvas canvas = holder.lockCanvas();

                if (canvas != null)
                {


                    canvas.drawARGB(255, 0, 0, 0);
                    try
                    {
                      drawClock(canvas);
                    } finally
                    {

                        holder.unlockCanvasAndPost(canvas);
                    }
                }

                // calculate the time required to draw the frame in ms


                // faster than the max fps - limit the FPS

                    try
                    {
                        Thread.sleep(100);
                    } catch (InterruptedException e)
                    {
                        // ignore
                    }

            }
        } catch (Exception e)
        {

        }

    }

    private void drawClock(Canvas canvas) {
        int width=canvas.getWidth();
        int height=canvas.getHeight();
        Point center=new Point(width/2,height/2);

        int radius= Math.min(width/2-2,height/2-2);
        for(int i=0;i<60;i++)
        {
            float x = (float) (center.x + radius * Math.cos(-i*6*Math.PI/180));
            float y = (float) (center.y + radius * Math.sin(-i*6*Math.PI/180));
            Paint paint=new Paint();
            paint.setStrokeWidth(3);
            paint.setARGB(255,255,45,89);
            canvas.drawPoint(x,y,paint);
        }
        Calendar calendar=Calendar.getInstance();
        String time="";
        float sec=calendar.get(Calendar.SECOND);
        float min=calendar.get(Calendar.MINUTE);
        float hour=calendar.get(Calendar.HOUR);

        if(sec<10)
            time="0"+String.valueOf((int)sec);
        else
            time=String.valueOf((int)sec);

        if(min<10)
            time="0"+String.valueOf((int)min)+":"+time;
        else
            time=String.valueOf((int)min)+":"+time;

        if(hour<10)
            time="0"+String.valueOf((int)hour)+":"+time;
        else
            time=String.valueOf((int)hour)+":"+time;

        if(calendar.get(Calendar.AM)==0)
            time=time+" AM";
        else
            time=time+" PM";
        sec=sec-15;
        min=min+((sec+15)/60)-15;
        hour=hour+((min+15)/60)-3;
        sec=sec*6;
        min=min*6;

        hour=hour*30;
        Paint paint = new Paint();
        paint.setTextSize(20);
        paint.setARGB(255,255,250,250);
        canvas.drawText(time,center.x/2,center.y,paint);
        float x = (float) (center.x + radius * Math.cos(sec*Math.PI/180)*.9f);
        float y = (float) (center.y + radius * Math.sin(sec*Math.PI/180)*.9f);
         paint=new Paint();
        paint.setStrokeWidth(3);
        paint.setARGB(255,255,45,89);
        canvas.drawLine(center.x,center.y,x,y,paint);
        x = (float) (center.x + radius * Math.cos(min*Math.PI/180)*.8f);
         y = (float) (center.y + radius * Math.sin(min*Math.PI/180)*.8f);
        paint=new Paint();
        paint.setStrokeWidth(3);
        paint.setARGB(255,0,250,89);
        canvas.drawLine(center.x,center.y,x,y,paint);
         x = (float) (center.x + radius * Math.cos(hour*Math.PI/180)*.7f);
         y = (float) (center.y + radius * Math.sin(hour*Math.PI/180)*.7f);
         paint=new Paint();
        paint.setStrokeWidth(3);
        paint.setARGB(255,255,220,89);
        canvas.drawLine(center.x,center.y,x,y,paint);

    }
}