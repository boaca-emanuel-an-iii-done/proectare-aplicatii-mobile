package com.example.ceas.SurfaceDrawer;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.TimePicker;

import com.example.ceas.AlarmRecivers.AlarmReciver;
import com.example.ceas.AlarmRecivers.TimerReciver;

import java.util.Calendar;

/**
 * Defines a custom SurfaceView class which handles the drawing thread
 **/
public class Timer extends SurfaceView implements SurfaceHolder.Callback, View.OnTouchListener, Runnable
{

    /**
     * Holds the surface frame
     */
    private SurfaceHolder holder;

    /**
     * Draw thread
     */
    private Thread drawThread;

    /**
     * True when the surface is ready to draw
     */
    private boolean surfaceReady = false;


    /**
     * Drawing thread flag
     */

    private boolean drawingActive = false;

    /**
     * Paint for drawing the sample rectangle
     */
    private Paint samplePaint = new Paint();

    /**
     * Time per frame for 60 FPS
     */

    private Context context;

    private static final String LOGTAG = "surface";

    public Timer(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        setOnTouchListener(this);
        this.context=context;
        // red
        samplePaint.setColor(0xffffff00);
        // smooth edges
       // samplePaint.setAntiAlias(true);
        samplePaint.setStrokeWidth(100f);
    }

    public Timer(Context context) {
        super(context);
        this.context=context;

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
    {
        if (width == 0 || height == 0)
        {
            return;
        }

        // resize your UI
    }
    public void set(Timer clk)
    {

    }
    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
        this.holder = holder;

        if (drawThread != null)
        {
            Log.d(LOGTAG, "draw thread still active..");
            drawingActive = false;
            try
            {
                drawThread.join();
            } catch (InterruptedException e)
            { // do nothing
            }
        }

        surfaceReady = true;
        startDrawThread();
        Log.d(LOGTAG, "Created");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        // Surface is not used anymore - stop the drawing thread
        stopDrawThread();
        // and release the surface
        holder.getSurface().release();

        this.holder = null;
        surfaceReady = false;
        Log.d(LOGTAG, "Destroyed");
    }

    @Override
    public boolean onTouch(View v, MotionEvent event)
    {
        // Handle touch events

        if(timeSelect) {
            timeSelect=false;
            TimePickerDialog tp = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    setStartTime(Calendar.getInstance());
                    setStopTime(Calendar.getInstance());
                   addTime(hourOfDay*60*60*1000+minute*60*1000);
                    timeSelect=true;

                }

            }, 0, 0, true);
            tp.setTitle("Set Alarm");
            tp.show();

        }
        return true;



    }
    Boolean timeSelect=true;
    /**
     * Stops the drawing thread
     */
    public void stopDrawThread()
    {
        if (drawThread == null)
        {
            Log.d(LOGTAG, "DrawThread is null");
            return;
        }
        drawingActive = false;
        while (true)
        {
            try
            {
                Log.d(LOGTAG, "Request last frame");
                drawThread.join(5000);
                break;
            } catch (Exception e)
            {
                Log.e(LOGTAG, "Could not join with draw thread");
            }
        }
        drawThread = null;
    }

    /**
     * Creates a new draw thread and starts it.
     */
    public void startDrawThread()
    {
        if (surfaceReady && drawThread == null)
        {
            drawThread = new Thread(this, "Draw thread");
            drawingActive = true;
            drawThread.start();
        }
    }

    @Override
    public void run()
    {
int chill=50;
        try
        {
            int time=0;
            while (drawingActive)
            {
                if (holder == null)
                {
                    return;
                }



                Canvas canvas = holder.lockCanvas();

                if (canvas != null)
                {


                    canvas.drawARGB(255, 0, 0, 0);
                    try
                    {
                      drawClock(canvas);
                    } finally
                    {

                        holder.unlockCanvasAndPost(canvas);
                    }
                }

                // calculate the time required to draw the frame in ms


                // faster than the max fps - limit the FPS

                    try
                    {
                        Thread.sleep(100);
                    } catch (InterruptedException e)
                    {
                        // ignore
                    }
                    if(!timeSelect)
                    { chill--;
                    if(chill<0)
                    {chill=50;
              timeSelect=true;}}
            }
        } catch (Exception e)
        {

        }

    }



    Calendar startTime=Calendar.getInstance();

    public Calendar getStartTime() {
        return startTime;
    }

    public void setStartTime(Calendar startTime) {
        this.startTime = startTime;

    }

    public Calendar getStopTime() {
        return stopTime;
    }

    private void setAlarm()
    {
        Intent alarm = new Intent(context, TimerReciver.class);
        PendingIntent pendingIntent=PendingIntent.getBroadcast(context,512,alarm,0);
        AlarmManager manager= (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
        manager.set(AlarmManager.RTC_WAKEUP,stopTime.getTimeInMillis(),pendingIntent);
    }

    public void setStopTime(Calendar stopTime) {
        this.stopTime = stopTime;
        this.startTime =Calendar.getInstance();
        setAlarm();

    }
    public void addTime(long amount) {
        int zile= (int) (amount/(24*60*60*1000));
        int ore= (int) (amount/(60*60*1000))%24;
        int min= (int) (amount/(60*1000))%60;
        int sec= (int) (amount/1000)%60;
        int mili= (int) (amount%1000);
        this.stopTime.add(Calendar.DAY_OF_YEAR,zile);
        this.stopTime.add(Calendar.HOUR_OF_DAY,ore);
        this.stopTime.add(Calendar.MINUTE,min);
        this.stopTime.add(Calendar.SECOND,sec);
        this.stopTime.add(Calendar.MILLISECOND,mili);
        setAlarm();
    }

    Calendar stopTime=Calendar.getInstance();
    private void drawClock(Canvas canvas) {
        int width=canvas.getWidth();
        int height=canvas.getHeight();
        Point center=new Point(width/2,height/2);

        int radius= Math.min(width/2-25,height/2-25);

        Calendar currentTime=Calendar.getInstance();
        float interval=(stopTime.getTimeInMillis()-startTime.getTimeInMillis());
        if(interval>0.0001f)
            interval=1000/interval;
        float indicator=interval*(stopTime.getTimeInMillis()-currentTime.getTimeInMillis());



        for(int i=0;i<1000;i++)
        {
            float x = (float) (center.x + radius *.95* Math.cos((-i-250)*Math.PI/500));
            float y = (float) (center.y + radius *.95* Math.sin((-i-250)*Math.PI/500));
            Paint paint=new Paint();
            paint.setStrokeWidth(2);
            paint.setARGB(255,255,45,255);
            if((int)(indicator)<=i)
            canvas.drawLine(center.x,center.y,x,y,paint);
             x = (float) (center.x + radius *1* Math.cos(-i*Math.PI/500));
             y = (float) (center.y + radius *1* Math.sin(-i*Math.PI/500));
            paint.setARGB(255,25,45,255);
            canvas.drawPoint(x,y,paint);
        }
        for(int i=0;i<12;i++)
        {
            float x = (float) (center.x + radius*.95* Math.cos(-i*30*Math.PI/180));
            float y = (float) (center.y + radius *.95* Math.sin(-i*30*Math.PI/180));
            Paint paint=new Paint();
            paint.setStrokeWidth(5);

            paint.setARGB(255,25,200,200);
            canvas.drawPoint(x,y,paint);

        }
        for(int j=3,i=0;i<12;j++,i++)
        {

            float x = (float) (center.x + radius*.85* Math.cos(i*30*Math.PI/180));
            float y = (float) (center.y + radius *.85* Math.sin(i*30*Math.PI/180));
            Paint paint=new Paint();
            if(j>12)
                j=1;
            paint.setTextSize(30);
            paint.setARGB(255,255,255,250);
            canvas.drawText(intToTime((int) (((1/interval)*j/12))),x,y,paint);

        }





    }
    String intToTime(int time)
    {
        int zi ,ora,minut,secunda;
        zi=time/(60*60*24);
        ora=time/(60*60)%24;
        minut=(time/60)%60;
        secunda=time%60;
        StringBuffer buffer=new StringBuffer();
        if(zi>0)
        {
            buffer.append(zi).append('-');
            if(ora<10)
                buffer.append(0);
            buffer.append(ora).append(':');
            if(minut<10)
                buffer.append(0);
            buffer.append(minut).append(':');
            if(secunda<10)
                buffer.append(0);
            buffer.append(secunda);
            return buffer.toString();

        }
        else
        if(ora>0)
        { buffer.append(ora).append(':');
        if(minut<10)
            buffer.append(0);
        buffer.append(minut).append(':');
        if(secunda<10)
            buffer.append(0);
        buffer.append(secunda);
        return buffer.toString();}
        else
        if(minut>0)
        {buffer.append(minut).append(':');
        if(secunda<10)
            buffer.append(0);
        buffer.append(secunda);
        return buffer.toString();}
        buffer.append(secunda);
        return buffer.toString();
    }
   }