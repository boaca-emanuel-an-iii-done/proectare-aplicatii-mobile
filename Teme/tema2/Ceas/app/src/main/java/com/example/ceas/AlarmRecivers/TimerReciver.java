package com.example.ceas.AlarmRecivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.widget.Toast;

public class TimerReciver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Vibrator v= (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        Toast.makeText(context,"Time's up !",Toast.LENGTH_LONG).show();
       v.vibrate(2000);

    }
}
