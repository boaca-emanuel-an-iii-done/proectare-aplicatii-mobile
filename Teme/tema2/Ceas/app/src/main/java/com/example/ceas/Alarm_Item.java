package com.example.ceas;

import java.util.Calendar;

public class Alarm_Item {
    Calendar ora;

    public Calendar getOra() {
        return ora;
    }

    public void setOra(Calendar ora) {
        this.ora = ora;
    }

    public int getAlarm_id() {
        return alarm_id;
    }

    public void setAlarm_id(int alarm_id) {
        this.alarm_id = alarm_id;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }

    public boolean isRepete() {
        return repete;
    }

    public void setRepete(boolean repete) {
        this.repete = repete;
    }

    public boolean isLuni() {
        return luni;
    }

    public void setLuni(boolean luni) {
        this.luni = luni;
    }

    public boolean isMarti() {
        return marti;
    }

    public void setMarti(boolean marti) {
        this.marti = marti;
    }

    public boolean isMiercuri() {
        return miercuri;
    }

    public void setMiercuri(boolean miercuri) {
        this.miercuri = miercuri;
    }

    public boolean isJoi() {
        return joi;
    }

    public void setJoi(boolean joi) {
        this.joi = joi;
    }

    public boolean isVineri() {
        return vineri;
    }

    public void setVineri(boolean vineri) {
        this.vineri = vineri;
    }

    public boolean isSambata() {
        return sambata;
    }

    public void setSambata(boolean sambata) {
        this.sambata = sambata;
    }

    public boolean isDuminica() {
        return duminica;
    }

    public void setDuminica(boolean duminica) {
        this.duminica = duminica;
    }

    public boolean isOn() {
        return on;
    }

    public void setOn(boolean on) {
        this.on = on;
    }

    int alarm_id;
    String sound;
    boolean repete,luni,marti,miercuri,joi,vineri,sambata,duminica,on;

}
