package com.example.ceas.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.ceas.Alarm_Item;
import com.example.ceas.R;

import java.util.ArrayList;
import java.util.List;

public class AlarmAdapter extends ArrayAdapter<Alarm_Item> {

    List<Alarm_Item> items;
    public AlarmAdapter(@NonNull Context context, int resource, @NonNull List<Alarm_Item> objects) {

        super(context, resource, objects);
        items=objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Alarm_Item item=getItem(position);
        if(convertView==null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.alarm_item,parent);
        }
        TextView ora=convertView.findViewById(R.id.Alarm_Time);
        Switch onswitch=convertView.findViewById(R.id.Alarm_on_off);
        TextView when=convertView.findViewById(R.id.When);
return convertView;

    }
}

