package com.example.ceas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.menu.ActionMenuItemView;
import androidx.fragment.app.Fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;



public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater =getMenuInflater();
        inflater.inflate(R.menu.topmeniu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId())
        {

            case R.id.Alarm_act:

                onAlarm();
                return true;
            case R.id.Ceas_act:
                onCeas();
                return true;
            case R.id.About_act:
                onAbout();
                return true;
            case R.id.Timer_act:
                onTimer();
                return true;
            case R.id.Settings_act:
                onSettings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
    @SuppressLint("RestrictedApi")
    void onAlarm(){

        resetIcon();
        ActionMenuItemView item = findViewById(R.id.Alarm_act);
        item.setIcon(getResources().getDrawable(R.drawable.ic_alarms_selected,null));
        findViewById(R.id.AlarmList).setVisibility(View.VISIBLE);
        findViewById(R.id.Clock).setVisibility(View.GONE);
        findViewById(R.id.Timer).setVisibility(View.GONE);

    }
    void onAbout(){

    }
    @SuppressLint("RestrictedApi")
    void onCeas(){
        resetIcon();
        ActionMenuItemView item = findViewById(R.id.Ceas_act);
        item.setIcon(getResources().getDrawable(R.drawable.ic_ceas_selected,null));
        findViewById(R.id.AlarmList).setVisibility(View.GONE);
        findViewById(R.id.Clock).setVisibility(View.VISIBLE);
        findViewById(R.id.Timer).setVisibility(View.GONE);

    }
    @SuppressLint("RestrictedApi")
    void onTimer(){
        resetIcon();
        ActionMenuItemView item = findViewById(R.id.Timer_act);
        item.setIcon(getResources().getDrawable(R.drawable.ic_timer_selected,null));
        findViewById(R.id.AlarmList).setVisibility(View.GONE);
        findViewById(R.id.Clock).setVisibility(View.GONE);
        findViewById(R.id.Timer).setVisibility(View.VISIBLE);
    }
    @SuppressLint("RestrictedApi")
    void onSettings(){
        resetIcon();
        ActionMenuItemView item = findViewById(R.id.Settings_act);
        item.setIcon(getResources().getDrawable(R.drawable.ic_settings_selected,null));
    }
    @SuppressLint("RestrictedApi")
    void resetIcon()
    {
        ActionMenuItemView item=findViewById(R.id.Ceas_act);
        item.setIcon(getResources().getDrawable( R.drawable.ic_ceas,null));
        item=findViewById(R.id.Alarm_act);
        item.setIcon(getResources().getDrawable(R.drawable.ic_alarm,null));
        item=findViewById(R.id.Timer_act);
        item.setIcon(getResources().getDrawable(R.drawable.ic_timer,null));
        item=findViewById(R.id.Settings_act);
        item.setIcon(getResources().getDrawable(R.drawable.ic_settings,null));
    }
}