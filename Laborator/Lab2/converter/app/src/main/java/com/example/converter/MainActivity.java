package com.example.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    // declare objects
    private Spinner fromSpinner  = null, toSpinner   = null;
    private EditText fromText    = null, toText      = null;
    private Button convertButton = null, clearButton = null;

    // give convertion data
    private String names [] = {"EUR", "RON", "UKP", "USD", "MDL"};
    private double rates [] = {1.0,   4.8,    0.89, 1.23,  21.2};
    private ArrayAdapter<String> adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // wire objects with widgets
        fromSpinner = findViewById(R.id.spinner1);
        toSpinner   = findViewById(R.id.spinner2);
        fromText    = findViewById(R.id.editText1);
        toText      = findViewById(R.id.editText2);

        clearButton = findViewById(R.id.button);
        convertButton=findViewById(R.id.button2);

        // make the spinners
        adapter = new ArrayAdapter<String>(
                getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                names);
        fromSpinner.setAdapter(adapter); toSpinner.setAdapter(adapter);

        // deal with buttons' events
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // reset edittexts and spinners
                fromText.setText(""); toText.setText("");

                fromSpinner.setSelection(0); toSpinner.setSelection(0);

            }
        });

        convertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get the value fromText and make it a double
                double fromValue = Double.parseDouble(fromText.getText().toString());

                // get the selection indices
                int fromIndex = fromSpinner.getSelectedItemPosition();
                int toIndex   = toSpinner.getSelectedItemPosition();

                // calculate the convertion rate
                double rate = rates[toIndex]/rates[fromIndex];

                // convert and put it to toEdit
                double toValue = fromValue * rate;

                toText.setText(""+toValue);

            }
        });

    }
}